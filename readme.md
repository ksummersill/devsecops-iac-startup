# Infrastructure as Code Startup
1. Setup Terrform
2. Setup Docker Servers with Terraform
3. Spring Boot Application Host on Tomcat Server

- [Student](documentation/student.md)


- [Instructor](documentation/instructor.md)
