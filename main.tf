# Configure the Docker provider - AKA Plugin
provider "docker" {
  host = "tcp://localhost:2375"
}

# Image Resource from Dockerhub
resource "docker_image" "tomcat" {
  name = "bitnami/tomcat:7.0.104"
}

# Container Resource
resource "docker_container" "tomcat-server" {
  image = "${docker_image.tomcat.name}"
  name  = "tomcat-server"
  ports {
    internal = 8080
    external = 8080
  }
}
